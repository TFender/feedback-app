﻿using System;
using System.Windows.Forms;

namespace Program
{
    public partial class Credentials : Form
    {

        private bool shutdown = true;

        public Credentials()
        {
            InitializeComponent();
            this.Text = "Login: ";
            label1.Text = "Voer username en wachtwoord in";
            this.CenterToScreen();
        }

        private void Credentials_Load(object sender, EventArgs e)
        {

        }

        private async void onOkAsync(object sender, EventArgs e)
        {
            if ((this.textBox2.Text == null || this.textBox2.Text == "") && (this.textBox1.Text == null || this.textBox1.Text == ""))
            {
                MessageBox.Show("Voer username en wachtwoord in", "Let op!");
                return;
            }
            if (this.textBox1.Text == null || this.textBox1.Text == "")
            {
                MessageBox.Show("Voer username in", "Let op!");
                return;
            }
            if (this.textBox2.Text == null || this.textBox2.Text == "")
            {
                MessageBox.Show("Voer wachtwoord in", "Let op!");
                return;
            }
            Issue.setUser(this.textBox1.Text);
            Issue.setPass(this.textBox2.Text);
            try
            {
                if (await Issue.testConnectionAsync())
                {
                    this.shutdown = false;
                    switchForms();
                } else
                {
                    MessageBox.Show("Foute credentials", "Let op!");
                }
            } catch (Exception)
            {
                MessageBox.Show("Fout bij inloggen", "Let op!");
            }
        }

        private void onClose(object sender, FormClosedEventArgs e)
        {
            if (this.shutdown)
            {
                Application.Exit();
            }
        }

        private void switchForms()
        {
            this.Hide();
            var tray = new Tray();
            tray.Closed += (s, args) => this.Close();
            tray.Show();
        }

        private void buttonKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                onOkAsync(sender, null);
            }
        }

        private void TextBoxKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                onOkAsync(sender, null);
            }
        }
    }
}
