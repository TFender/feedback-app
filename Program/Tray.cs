﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Program
{
    class Tray : Form
    {
        private NotifyIcon trayIcon;
        private ContextMenu trayMenu;
        private GlobalKeyboardHook _globalKeyboardHook;
        public static Boolean inProgress = false;

        public Tray()
        {
            trayMenu = new ContextMenu();
            trayMenu.MenuItems.Add("Exit", OnExit);
            trayIcon = new NotifyIcon();
            trayIcon.Text = "FeedbackApp";
            trayIcon.Icon = new Icon(SystemIcons.Application, 40, 40);
            trayIcon.ContextMenu = trayMenu;
            trayIcon.Visible = true;
            SetupKeyboardHooks();
        }

        protected override void OnLoad(EventArgs e)
        {
            Visible = false;
            ShowInTaskbar = false;
            base.OnLoad(e);
        }

        private void OnExit(object sender, EventArgs e)
        {
            this.Dispose();
            Application.Exit();
        }

        public void SetupKeyboardHooks()
        {
            _globalKeyboardHook = new GlobalKeyboardHook();
            _globalKeyboardHook.KeyboardPressed += OnKeyPressed;
        }

        private void OnKeyPressed(object sender, GlobalKeyboardHookEventArgs e)
        {
            if (e.KeyboardData.VirtualCode != GlobalKeyboardHook.VkSnapshot)
                return;

            if (e.KeyboardState == GlobalKeyboardHook.KeyboardState.SysKeyDown &&
             e.KeyboardData.Flags == GlobalKeyboardHook.LlkhfAltdown)
            {
                if (!inProgress)
                {
                    inProgress = true;
                    foreach (Screen screen in Screen.AllScreens)
                    {
                        new TakeScreenshot(screen);
                    }
                    e.Handled = true;
                }
            }  
        }
    
    protected override void Dispose(bool isDisposing)
        {
            if (isDisposing)
            {
                trayIcon.Dispose();
            }
        _globalKeyboardHook?.Dispose();
        base.Dispose(isDisposing);
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            this.ClientSize = new System.Drawing.Size(282, 253);
            this.Name = "Tray";
            this.ShowInTaskbar = false;
            this.ResumeLayout(false);

        }
    }
}
