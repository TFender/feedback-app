﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Program
{
    public partial class Form1 : Form
    {
        private Boolean isBlack = false;
        private Boolean isMarker = false;
        private Boolean isPen = false;
        private Boolean isMouseDown = false;
        private Boolean isTakingSecondScreenshot = false;

        private Dictionary<String, String> EpicList = null;

        private Point mouseDownPoint = Point.Empty;
        private Point mousePoint = Point.Empty;
        private Point tempPoint = Point.Empty;

        private static List<Bitmap> origImages = new List<Bitmap>();
        private static List<Bitmap> fitImages = new List<Bitmap>();
        private List<String> filePaths = new List<String>();

        private static int index = 0;
        private static String Summary;
        private static String Description;

        private Rectangle tempRect;

        public Form1()
        {
            InitializeComponent();
            this.bindingSource1.DataSource = EpicList;
            this.comboBox1.DataSource = this.bindingSource1.DataSource;
            this.comboBox1.Text = "Selecteer Applicatie";
            this.textBox1.Text = Summary;
            this.richTextBox1.Text = Description;
            new Task(() => getEpicList()).Start();
        }

        public void setImage(Bitmap image)
        {
            Boolean isExtistingScreenshot = true;
            if (!origImages.Contains(image) && origImages.Count == 0)
            {
                isExtistingScreenshot = false;
                origImages.Add(image);
                index = 0;
            }
            else if (!origImages.Contains(image))
            {
                isExtistingScreenshot = false;
                origImages.Add(image);
                index = origImages.IndexOf(image);
            }
            double curW = this.pictureBox1.Width;
            double curH = this.pictureBox1.Height;
            double origW = origImages[index].Width;
            double origH = origImages[index].Height;
            double maxOrig = (double)Math.Max(origW, origH);
            double ratio = (maxOrig == origH)? curH / origH : curW / origW;
            Bitmap newImage = new Bitmap(origImages[index], new Size((int)(image.Width * ratio),(int)(image.Height * ratio)));
            this.pictureBox1.Image = newImage;
            setLabelText();
            if (isExtistingScreenshot)
            {
                fitImages[index] = (newImage);
            } else
            {
                fitImages.Add(newImage);
            }
        }

        private void black_Click(object sender, EventArgs e)
        {
            isBlack = true;
            isMarker = false;
            isPen = false;
        }

        private void mark_Click(object sender, EventArgs e)
        {
            isBlack = false;
            isMarker = true;
            isPen = false;
        }

        private void pen_click(object sender, EventArgs e)
        {
            isBlack = false;
            isMarker = false;
            isPen = true;
        }

        private void send_Click(object sender, EventArgs e)
        {
            if (this.comboBox1.SelectedItem == null)
            {
                MessageBox.Show("Selecteer Applicatie", "Ho eens even!");
                return;
            }
            if (this.textBox1.Text == null || this.textBox1.Text.Length == 0)
            {
                MessageBox.Show("Vul een onderwerp in", "Ho eens even!");
                return;
            }
            if (this.richTextBox1.Text == null || this.richTextBox1.Text.Length == 0)
            {
                MessageBox.Show("Vul een omschrijving in", "Ho eens even!");
                return;
            }
            for (int i=0; i< fitImages.Count;i++)
            {
                if (fitImages[i] != null)
                {
                    String filename = Directory.GetCurrentDirectory() + "\\tmpimg\\" + this.comboBox1.SelectedItem.ToString() + "-Image-" + (i + 1) + ".png";
                    fitImages[i].Save(filename);
                    this.filePaths.Add(filename);
                }
            }
            String EpicKey = this.EpicList[this.comboBox1.SelectedItem.ToString()];
            String Summary = this.textBox1.Text;
            String Description = this.richTextBox1.Text;
            new Task(() => Issue.addIssue(EpicKey,Summary,Description, filePaths.ToArray())).Start();
            MessageBox.Show("Bedankt voor de opbouwende kritieken", "Bedankt");
            this.Close();
        }

        private void cancel_Click(object sender, EventArgs e)
        {
            removeScreenShots();
            this.Close();
        }

        private void pictureMouseDown(object sender, MouseEventArgs e)
        {
            this.pictureBox1.Refresh();
            isMouseDown = true;
            mousePoint = mouseDownPoint = e.Location;
        }

        private void pictureMouseMove(object sender, MouseEventArgs e)
        {
            mousePoint = e.Location;
            if ((isBlack || isMarker) && this.isMouseDown)
            {
                this.pictureBox1.Refresh();
                double tempWidth = Math.Abs(mouseDownPoint.X - mousePoint.X);
                double tempHeight = Math.Abs(mouseDownPoint.Y - mousePoint.Y);
                Brush brush = new SolidBrush(Color.FromArgb(25, 165, 175, 183));
                tempRect = new Rectangle((int)(Math.Min(mouseDownPoint.X, mousePoint.X)), (int)(Math.Min(mouseDownPoint.Y, mousePoint.Y)), (int)(tempWidth), (int)(tempHeight));
                pictureBox1.CreateGraphics().FillRectangle(brush,tempRect);
            }
            if (isPen && this.isMouseDown)
            {
                Bitmap map = fitImages[index];
                Graphics g = Graphics.FromImage(fitImages[index]);

                Pen redPen = new Pen(Color.FromArgb(135, 255, 25, 10), 5);
                if (tempPoint.IsEmpty)
                {
                    tempPoint = mouseDownPoint;
                }
                g.SmoothingMode = SmoothingMode.AntiAlias;
                g.DrawLine(redPen, tempPoint, mousePoint);
                tempPoint = mousePoint;
                pictureBox1.Image = map;
                this.pictureBox1.Invalidate();
                g.Dispose();
            }
        }

        private void pictureMouseUp(object sender, MouseEventArgs e)
        {
            if (isMouseDown)
            {
                double width = Math.Abs(mouseDownPoint.X - mousePoint.X);
                double height = Math.Abs(mouseDownPoint.Y - mousePoint.Y);
                pictureBox1.Refresh();
                Bitmap map = fitImages[index];
                Graphics g = Graphics.FromImage(map);
                Brush brush = null;
                if (isBlack) {
                    brush = new SolidBrush(Color.FromArgb(255, 0, 0, 0));
                    g.FillRectangle(brush, new Rectangle((int)(Math.Min(mouseDownPoint.X, mousePoint.X)), (int)(Math.Min(mouseDownPoint.Y, mousePoint.Y)), (int)(width), (int)(height)));

                }
                if (isMarker)
                {
                    brush = new SolidBrush(Color.FromArgb(80, 255, 249, 104));
                    g.FillRectangle(brush, new Rectangle((int)(Math.Min(mouseDownPoint.X, mousePoint.X)), (int)(Math.Min(mouseDownPoint.Y, mousePoint.Y)), (int)(width), (int)(height)));

                }
                pictureBox1.Image = map;
                g.Dispose();
                this.isMouseDown = false;
                this.tempPoint = Point.Empty;
                this.mouseDownPoint = Point.Empty;
                this.mousePoint = Point.Empty;
            }
        }        

        private async void getEpicList()
        {
            try
            {
                EpicList = await Issue.getEpicList();
            } catch (Exception)
            {
                MessageBox.Show("niet mogelijk om Epics van JIRA op te halen", "Helaas");
            }
        }

        private void ClearAll(object sender, EventArgs e)
        {
            this.setImage(origImages[index]);
            this.pictureBox1.Invalidate();
            this.pictureBox1.Refresh();
        }

        private void onRight(object sender, EventArgs e)
        {
            if ((index + 1) == origImages.Count)
            {
                return;
            }
            index++;
            setLabelText();
            this.pictureBox1.Image = fitImages[index];
        }

        private void onLeft(object sender, EventArgs e)
        {
            if (index == 0)
            {
                return;
            }
            index--;
            setLabelText();
            this.pictureBox1.Image = fitImages[index];
        }

        private void onDeleteImage(object sender, EventArgs e)
        {
            if (origImages.Count > 1)
            {
                origImages.RemoveAt(index);
                fitImages.RemoveAt(index);
                MessageBox.Show("Screenshot verwijderd uit selectie", "Let op!");
                if (index == origImages.Count)
                {
                    index--;
                }
                this.pictureBox1.Image = fitImages[index];
                this.setLabelText();
            }
            else
            {
                MessageBox.Show("Maar 1 screenshot in selectie, kan niet verwijderen", "Let op!");
            }
        }

        private void onAddScreenshot(object sender, EventArgs e)
        {
            MessageBox.Show("Dit venster sluit, er kan een extra screenshot worden genomen met Alt+PrtScr", "Let op!");
            Summary = this.textBox1.Text;
            Description = this.richTextBox1.Text;
            this.isTakingSecondScreenshot = true;
            this.Close();
        }

        private void setLabelText()
        {
            this.label1.Text = (index + 1) + " / " + origImages.Count;
        }

        public static void removeScreenShots()
        {
            string[] filePaths = Directory.GetFiles(Directory.GetCurrentDirectory()+"\\tmpimg");
            foreach (string filePath in filePaths)
                if (filePath.EndsWith(".png"))
                {
                    File.Delete(filePath);
                }
        }

        private void OnApplicationClick(object sender, EventArgs e)
        {
            if (EpicList != null && this.comboBox1.Items.Count==0)
            {
                foreach(var i in EpicList)
                {
                    this.comboBox1.Items.Add(i.Key);
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void formClosing(object sender, FormClosedEventArgs e)
        {
            if (!isTakingSecondScreenshot)
            {
                origImages.Clear();
                this.filePaths.Clear();
                fitImages.Clear();
                Summary = null;
                Description = null;
                MessageBox.Show("Willem van der Tuuk & Ruud Westendorp, 22-02-2018", "Feedback v0.6");
            }
        }
    }
}
