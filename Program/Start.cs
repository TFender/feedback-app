﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Program
{
    static class Start
    {
        [STAThread]
        static void Main()
        {
            Directory.CreateDirectory("tmpimg");
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Dpi.SetDpiAwareness();
            Application.Run(new Credentials());
        }
    }
}
