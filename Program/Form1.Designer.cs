﻿namespace Program
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Opties = new System.Windows.Forms.GroupBox();
            this.clearButton = new System.Windows.Forms.Button();
            this.penButton = new System.Windows.Forms.Button();
            this.cancel = new System.Windows.Forms.Button();
            this.Send = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.markButton = new System.Windows.Forms.Button();
            this.blackButton = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.leftButton = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.addButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.deleteButton = new System.Windows.Forms.Button();
            this.rightButton = new System.Windows.Forms.Button();
            this.Opties.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // Opties
            // 
            this.Opties.Controls.Add(this.clearButton);
            this.Opties.Controls.Add(this.penButton);
            this.Opties.Controls.Add(this.cancel);
            this.Opties.Controls.Add(this.Send);
            this.Opties.Controls.Add(this.groupBox3);
            this.Opties.Controls.Add(this.groupBox2);
            this.Opties.Controls.Add(this.groupBox1);
            this.Opties.Controls.Add(this.markButton);
            this.Opties.Controls.Add(this.blackButton);
            this.Opties.Location = new System.Drawing.Point(963, 6);
            this.Opties.Margin = new System.Windows.Forms.Padding(4);
            this.Opties.Name = "Opties";
            this.Opties.Padding = new System.Windows.Forms.Padding(4);
            this.Opties.Size = new System.Drawing.Size(497, 806);
            this.Opties.TabIndex = 0;
            this.Opties.TabStop = false;
            this.Opties.Text = "Opties";
            // 
            // clearButton
            // 
            this.clearButton.BackColor = System.Drawing.Color.PaleTurquoise;
            this.clearButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clearButton.Location = new System.Drawing.Point(376, 23);
            this.clearButton.Margin = new System.Windows.Forms.Padding(4);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(112, 62);
            this.clearButton.TabIndex = 8;
            this.clearButton.Text = "Wis";
            this.clearButton.UseVisualStyleBackColor = false;
            this.clearButton.Click += new System.EventHandler(this.ClearAll);
            // 
            // penButton
            // 
            this.penButton.BackColor = System.Drawing.Color.Tomato;
            this.penButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.penButton.Location = new System.Drawing.Point(256, 23);
            this.penButton.Margin = new System.Windows.Forms.Padding(4);
            this.penButton.Name = "penButton";
            this.penButton.Size = new System.Drawing.Size(112, 62);
            this.penButton.TabIndex = 7;
            this.penButton.Text = "Pen";
            this.penButton.UseVisualStyleBackColor = false;
            this.penButton.Click += new System.EventHandler(this.pen_click);
            // 
            // cancel
            // 
            this.cancel.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cancel.Location = new System.Drawing.Point(255, 736);
            this.cancel.Margin = new System.Windows.Forms.Padding(4);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(235, 62);
            this.cancel.TabIndex = 6;
            this.cancel.Text = "Annuleer";
            this.cancel.UseVisualStyleBackColor = false;
            this.cancel.Click += new System.EventHandler(this.cancel_Click);
            // 
            // Send
            // 
            this.Send.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Send.Location = new System.Drawing.Point(12, 736);
            this.Send.Margin = new System.Windows.Forms.Padding(4);
            this.Send.Name = "Send";
            this.Send.Size = new System.Drawing.Size(235, 62);
            this.Send.TabIndex = 5;
            this.Send.Text = "Verzend";
            this.Send.UseVisualStyleBackColor = false;
            this.Send.Click += new System.EventHandler(this.send_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.richTextBox1);
            this.groupBox3.Location = new System.Drawing.Point(12, 240);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(476, 488);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Omschrijving";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox1.Location = new System.Drawing.Point(8, 23);
            this.richTextBox1.Margin = new System.Windows.Forms.Padding(4);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(455, 457);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.comboBox1);
            this.groupBox2.Location = new System.Drawing.Point(8, 92);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(480, 68);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Applicatie";
            // 
            // comboBox1
            // 
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(8, 25);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(4);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(459, 28);
            this.comboBox1.TabIndex = 0;
            this.comboBox1.Click += new System.EventHandler(this.OnApplicationClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Location = new System.Drawing.Point(8, 167);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(480, 65);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Onderwerp";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(8, 23);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(459, 27);
            this.textBox1.TabIndex = 0;
            // 
            // markButton
            // 
            this.markButton.BackColor = System.Drawing.SystemColors.Info;
            this.markButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.markButton.Location = new System.Drawing.Point(136, 23);
            this.markButton.Margin = new System.Windows.Forms.Padding(4);
            this.markButton.Name = "markButton";
            this.markButton.Size = new System.Drawing.Size(112, 62);
            this.markButton.TabIndex = 1;
            this.markButton.Text = "Markeer";
            this.markButton.UseVisualStyleBackColor = false;
            this.markButton.Click += new System.EventHandler(this.mark_Click);
            // 
            // blackButton
            // 
            this.blackButton.BackColor = System.Drawing.Color.DimGray;
            this.blackButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.blackButton.Location = new System.Drawing.Point(16, 23);
            this.blackButton.Margin = new System.Windows.Forms.Padding(4);
            this.blackButton.Name = "blackButton";
            this.blackButton.Size = new System.Drawing.Size(112, 62);
            this.blackButton.TabIndex = 0;
            this.blackButton.Text = "Censureer";
            this.blackButton.UseVisualStyleBackColor = false;
            this.blackButton.Click += new System.EventHandler(this.black_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(16, 15);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(939, 719);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureMouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureMouseMove);
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureMouseUp);
            // 
            // leftButton
            // 
            this.leftButton.BackColor = System.Drawing.SystemColors.ControlLight;
            this.leftButton.Location = new System.Drawing.Point(316, 14);
            this.leftButton.Margin = new System.Windows.Forms.Padding(4);
            this.leftButton.Name = "leftButton";
            this.leftButton.Size = new System.Drawing.Size(70, 49);
            this.leftButton.TabIndex = 9;
            this.leftButton.Text = "<";
            this.leftButton.UseVisualStyleBackColor = false;
            this.leftButton.Click += new System.EventHandler(this.onLeft);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.addButton);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.deleteButton);
            this.groupBox4.Controls.Add(this.leftButton);
            this.groupBox4.Controls.Add(this.rightButton);
            this.groupBox4.Location = new System.Drawing.Point(16, 742);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(939, 70);
            this.groupBox4.TabIndex = 10;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "navigeer screenshots";
            // 
            // addButton
            // 
            this.addButton.BackColor = System.Drawing.SystemColors.ControlLight;
            this.addButton.Location = new System.Drawing.Point(648, 14);
            this.addButton.Margin = new System.Windows.Forms.Padding(4);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(138, 49);
            this.addButton.TabIndex = 14;
            this.addButton.Text = "Neem Extra screenshot";
            this.addButton.UseVisualStyleBackColor = false;
            this.addButton.Click += new System.EventHandler(this.onAddScreenshot);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(414, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 36);
            this.label1.TabIndex = 13;
            this.label1.Text = "1/1";
            // 
            // deleteButton
            // 
            this.deleteButton.BackColor = System.Drawing.SystemColors.ControlLight;
            this.deleteButton.Location = new System.Drawing.Point(794, 14);
            this.deleteButton.Margin = new System.Windows.Forms.Padding(4);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(138, 49);
            this.deleteButton.TabIndex = 12;
            this.deleteButton.Text = "Verwijder huidig screenshot";
            this.deleteButton.UseVisualStyleBackColor = false;
            this.deleteButton.Click += new System.EventHandler(this.onDeleteImage);
            // 
            // rightButton
            // 
            this.rightButton.BackColor = System.Drawing.SystemColors.ControlLight;
            this.rightButton.Location = new System.Drawing.Point(502, 14);
            this.rightButton.Margin = new System.Windows.Forms.Padding(4);
            this.rightButton.Name = "rightButton";
            this.rightButton.Size = new System.Drawing.Size(70, 49);
            this.rightButton.TabIndex = 11;
            this.rightButton.Text = ">";
            this.rightButton.UseVisualStyleBackColor = false;
            this.rightButton.Click += new System.EventHandler(this.onRight);
            // 
            // Form1
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1473, 825);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.Opties);
            this.Controls.Add(this.groupBox4);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Feedback-app v0.6";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.formClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Opties.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox Opties;
        private System.Windows.Forms.Button blackButton;
        private System.Windows.Forms.Button markButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button cancel;
        private System.Windows.Forms.Button Send;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.Button penButton;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.Button leftButton;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button rightButton;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button deleteButton;
    }
}