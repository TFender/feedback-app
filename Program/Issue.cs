﻿using Atlassian.Jira;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows;

namespace Program
{
    public class Issue
    {
        private static String url = "https://type22.atlassian.net/";
        private static String user = "";
        private static String pass = "";
        private static String key = "ITNFB";

        public static void setPass(String password)
        {
            pass = password;
        }
        
        public static void setUser(String username)
        {
            user = username;
        }

        public static String getUser()
        {
            return user;
        }

        public async static Task<bool> testConnectionAsync()
        {
            try
            {
                await getEpicList();
            } catch (Exception)
            {
                return false;
            }
            return true;
        }

        public static async Task addIssue(String EpicKey, String Summary, String Description, String[] filePaths)
        {
            try
            {
                var jira = Jira.CreateRestClient(url,user,pass);
                jira.RestClient.RestSharpClient.Proxy = WebRequest.GetSystemWebProxy();
                jira.RestClient.RestSharpClient.Proxy.Credentials = CredentialCache.DefaultCredentials;
                var issue = new Atlassian.Jira.Issue(jira, key);
                issue.Type = "Story";
                issue.Summary = Summary;
                issue.Description = Description;
                issue["Epic Link"] = EpicKey;
                issue.SaveChanges();
                issue.AddAttachment(filePaths);
                await issue.SaveChangesAsync();
                Form1.removeScreenShots();
            } catch (Exception e)
            {
                MessageBox.Show("Fout bij connectie met JIRA: "+e.ToString());
            }
        }

        public static async Task<Dictionary<String, String>> getEpicList()
        {
            var jira = Jira.CreateRestClient(url, user, pass);
            jira.RestClient.RestSharpClient.Proxy = WebRequest.GetSystemWebProxy();
            jira.RestClient.RestSharpClient.Proxy.Credentials = CredentialCache.DefaultCredentials;

            var issues = from i in jira.Issues.Queryable
                         where (i.Type == "Epic" && i.Project==key)
                         select i;

            Dictionary<String, String> EpicList = new Dictionary<string, string>();

            foreach(var issue in issues)
            {
                if (issue.CustomFields.Count > 2)
                {
                    EpicList.Add(issue.CustomFields[4].Values[0], issue.Key.ToString());
                }
            }

            return EpicList;
        }
    }
}
