﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;

namespace Program
{
    public partial class TakeScreenshot : Window
    {
        private double width;
        private double height;
        private bool isMouseDown = false;
        private Screen screen;

        private static List<TakeScreenshot> takeScreenshotList = new List<TakeScreenshot>();

        System.Windows.Point mouseDownPoint = new System.Windows.Point();
        System.Windows.Point mousePoint = new System.Windows.Point();

        public TakeScreenshot(Screen screen)
        {
            InitializeComponent();
            this.screen = screen;
            takeScreenshotList.Add(this);
            var workingArea = screen.Bounds;
            this.Left = workingArea.Left;
            this.Top = workingArea.Top;
            this.WindowState = WindowState.Minimized;
            this.Show();
            this.WindowState = WindowState.Normal;
            this.WindowState = WindowState.Maximized;
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Dpi.SetDpiAwareness();
            isMouseDown = true;
            mousePoint = mouseDownPoint = e.GetPosition(null);
        }

        private void Window_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (isMouseDown)
            {
                mousePoint = e.GetPosition(null);

                System.Windows.Shapes.Rectangle r = new System.Windows.Shapes.Rectangle();
                SolidColorBrush brush = new SolidColorBrush(Colors.White);
                r.Stroke = brush;
                r.Fill = brush;
                r.StrokeThickness = -1;
                r.Width = Math.Abs(mouseDownPoint.X - mousePoint.X);
                r.Height = Math.Abs(mouseDownPoint.Y - mousePoint.Y);
                cnv.Children.Clear();
                cnv.Children.Add(r);
                Canvas.SetLeft(r, Math.Min(mouseDownPoint.X, mousePoint.X));
                Canvas.SetTop(r, Math.Min(mouseDownPoint.Y, mousePoint.Y));
                if (e.LeftButton == MouseButtonState.Released)
                {
                    width = Math.Abs(mouseDownPoint.X - mousePoint.X);
                    height = Math.Abs(mouseDownPoint.Y - mousePoint.Y);
                    if (width > 10 && height > 10)
                    {
                        System.Windows.Point startPoint = PointToScreen(new System.Windows.Point((Math.Min(mouseDownPoint.X, mousePoint.X)), (Math.Min(mouseDownPoint.Y, mousePoint.Y))));
                        System.Windows.Point endPoint = PointToScreen(new System.Windows.Point((Math.Max(mouseDownPoint.X, mousePoint.X)), (Math.Max(mouseDownPoint.Y, mousePoint.Y))));
                        width = Math.Abs(startPoint.X - endPoint.X);
                        height = Math.Abs(startPoint.Y - endPoint.Y);
                        this.CaptureScreen((int)startPoint.X, (int)startPoint.Y, (int)width, (int)height);
                    }
                    else
                    {
                        System.Windows.MessageBox.Show("te kleine selectie");
                    }
                        isMouseDown = false;
                        Tray.inProgress = false;
                }
            }
        }
        private void CaptureScreen(int x, int y, int width, int height)
        {
            Bitmap image = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            Graphics g = Graphics.FromImage(image);
            g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
            g.CopyFromScreen(x, y, 0, 0, image.Size, CopyPixelOperation.SourceCopy);
            killTakeScreenshot();
            Form1 form1 = new Form1();
            form1.StartPosition = FormStartPosition.CenterScreen;
            form1.setImage(image);
            form1.ShowDialog();
        }


        private void killTakeScreenshot()
        {
            foreach (var t in takeScreenshotList)
            {
                t.Close();
            }
        }

    }
}
